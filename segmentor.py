#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A script that tries to select fishing places

"""

################ IMPORTS #############################################3
import folium
import pandas as pd
import gpxpy
from gpxpy import gpx as GPX
from math import sqrt
import numpy as np
import json
from sys import argv
from math import inf, sin, cos, asin, acos, pi, radians, degrees, atan, exp
import seanoodle.seanoodle as seanoodle

################## CONSTANTS #########################################
NAUTICAL_MILE_IN_M = 1852
EARTH_RADIUS = 6371  #in km
CP_DETECT_ZONE = 7  # in nautical miles

COLOR_CODE = {
    "fishing": "#00FF00",
    "control": "red",
    "enroute": "blue",
}

POSITIVE_ACTIVITIES = ["fishing"]
NEGATIVE_ACTIVITIES = ["control"]

######################## CLASSES ###################################


class SpecializedSegment(GPX.GPXTrackSegment):
    """
    Vanilla GPX track segments don't have any metadata fields.
    So we add them to not get crazy and have everything in pne place.
    
    """
    def __init__(self, role="enroute"):
        super().__init__()
        self.role = role

class controlPoint(object):
    """
    Control point which is mandatory to visit when boat switches areas.
    Note that it will pass for a gpx_point by parameter names
    """
    def __init__(self, lat=0, long=0, name="kp", radius=2):
        self.latitude = lat
        self.longitude = long
        self.name = name
        self.radius = radius  #in

    def _asdict(self): # probably redundant in this particular script. is needed for json-ing. 
        return self.__dict__

    def to_circle(self):
        """
        returns visual entity to attach to a map
        """
        circle = folium.Circle([self.latitude, self.longitude],
                               self.radius * NAUTICAL_MILE_IN_M,
                               color='crimson',
                               tooltip="Control point:  " + self.name)
        return circle

    def to_detection_zone(self):
        """
        returns visual entity to attach to a map
        """
        circle = folium.Circle([self.latitude, self.longitude],
                               CP_DETECT_ZONE * NAUTICAL_MILE_IN_M,
                               color='black',
                               tooltip="Control point detection zone:  " +
                               self.name)
        return circle

    def is_in_detection_range(self, gpx_point):
        """
        Checks if a trackpoint can be considered "visiting" a control point.
        """
        #print (gpx_distance(self,gpx_point))
        return (gpx_distance(self, gpx_point) <
                CP_DETECT_ZONE * NAUTICAL_MILE_IN_M / 1000)


##################### FUNCTIONS ################

###################### Visual #############################

def prepare_blank_map(gpx):
        
    bb = gpx.get_bounds()
    m = folium.Map()
    m.fit_bounds([[bb.max_latitude, bb.max_longitude],
                  [bb.min_latitude, bb.min_longitude]])
    return m

def pointvals_to_legvals(array):
    """
    converts n values of vertices to n-1 values of edges for colorful display
    """
    assert len(array) > 1
    return [(array[i] + array[i - 1]) / 2 for i in range(1, len(array))]

def circle_marker(gpx_point, **kwargs):
    """
      Makes a circle marker with arbitrary values listed in the tooltip
      """

    abbreviations = {
        "time": "",
        "latitude": "lat:",
        "longitude": "long:",
        "course": "c:",
        "speed": "s:",
    }

    normal_attr_strings = []
    for attr in abbreviations.keys():
        value = getattr(gpx_point, attr)
        attr_string = "%s   %s \n" % (abbreviations[attr], value)
        normal_attr_strings.append(attr_string)

    for name, value in kwargs.items():
        attr_string = "%s   %s \n" % (name.capitalize(), value)
        normal_attr_strings.append(attr_string)

    tooltip_text = "<br>".join(normal_attr_strings)
    return folium.CircleMarker([gpx_point.latitude, gpx_point.longitude],
                               radius=3,
                               tooltip=tooltip_text,
                               popup=tooltip_text,
                               opacity=0.0)


def display_optimized_route(gpx_route, original_length_km, _map):
    """
    Make a polyline on a map with some optimisation info and display it.
    Represents a route optimised by a routing engine.
    """
    color = "magenta"
    optimized_length = series_distance(gpx_route.points)
    difference = original_length_km - optimized_length
    tooltip = ''' 
                Original length: %f km <br>
                Optimized legth: %f km <br>
                Difference: %f km
                ''' % (original_length_km, optimized_length, difference)
    #print(role, len(segment.points))
    pl = folium.PolyLine(
        [gpx_to_location(point) for point in gpx_route.points],
        color=color,
        tooltip=tooltip)
    pl.add_to(_map)

def display_segments_with_tasks(classified_segments, _map):
    for i, segment in enumerate(classified_segments):
        role = segment.role
        color = COLOR_CODE[role]
        #print(role, len(segment.points))
        pl = folium.PolyLine([gpx_to_location(point) for point in segment.points],
                             color=color,
                             tooltip=role + " index: " + str(i))
        pl.add_to(_map)

def add_control_point_visuals(control_points, _map):
    for cp in [x.to_circle() for x in control_points]:
        cp.add_to(_map)

    ## Debug view detection zone
    for cp in [x.to_detection_zone() for x in control_points]:
        cp.add_to(_map)

def make_a_map(control_points,df,zonespecs,classified_segments,optimised_routes): #argument number can obviously be reduced
    """
    Output a map
    """
    
    ## temporary cruft.
    gpx=GPX.GPX()
    initial_track = GPX.GPXTrack()
    gpx_segment = pandas_to_segment(df)
    initial_track.segments.append(gpx_segment)
    gpx.tracks.append(initial_track)
    
    m = prepare_blank_map(gpx)
    add_control_point_visuals(control_points, m)
    
    l_df = extra_label_df(df,zonespecs)
    circle_markers = []
    for i, point in enumerate(gpx_segment.points):
        dic = l_df.iloc[i].to_dict()
        for zone in zonespecs:
            dic[zone.name+'_'+'threshold'] = zone.thres
        circle_markers.append(circle_marker(point,**dic))
        
    for marker in circle_markers:
        marker.add_to(m)
    
    ### display new segments
    display_segments_with_tasks(classified_segments,m)
    for route in optimised_routes:
        display_optimized_route(route,
                                    float(route.description.split(sep=":")[1]), m)
    
    return m

##################### IO ###################################
def usage():
    print("Usage:\t %s INFILE.gpx OUTFILE.html " % argv[0])


def load_control_points(filename):
    """
    Auxiliary file handing function to load control points from a json fle. 
    """
    file = open(filename)
    points = json.load(file)
    cps = []
    for point in points:
        cp = controlPoint()
        for attr in point.keys():
            setattr(cp, attr, point[attr])
        cps.append(cp)
    return cps

##################### Geodesy #############################

def great_angle(lat1, long1, lat2, long2):
    """
    Great circle angle
    https://en.wikipedia.org/wiki/Great-circle_distance
    haversine formula. Always positive.
    """

    delta_lat = angle_difference(
        lat2, lat1, 2 * pi)  #matters only because we are dividing by 2
    delta_long = angle_difference(long2, long1, 2 * pi)

    angle = 2 * asin(
        sqrt(
            pow(sin(delta_lat / 2), 2) +
            cos(lat1) * cos(lat2) * pow(sin(delta_long / 2), 2)))
    return angle


def gpx_distance(point1, point2):
    """
    distance between 2 points in km
    """
    angle = great_angle(radians(point1.latitude), radians(point1.longitude),
                        radians(point2.latitude), radians(point2.longitude))
    distance = EARTH_RADIUS * angle
    return distance

def true_course_between_pts(point1, point2):
    """
    https://en.wikipedia.org/wiki/Great-circle_navigation.
    A true course of a ship travelling from A to B on a great circle.
    In the point A.
    """

    lat1 = radians(point1.latitude)
    lat2 = radians(point2.latitude)
    long1 = radians(point1.longitude)
    long2 = radians(point2.longitude)

    long12 = long2 - long1

    ## Calculating tangent of a true course
    dividend = (cos(lat2) * sin(long12))
    divisor = (cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(long12))
    if divisor == 0:
        return 0.

    tang = dividend / divisor
    ##deal with atan ambiguity
    course = degrees(atan(tang))  # from -90 to +90

    #    print("raw from atan", course)

    if lat2 < lat1:
        course = 180 + course
#        print("lat2<lat1, new course", course)

##converting to 0-360
    if course < 0:
        course = 360 + course


#    print("final course", course)

    if long12 > pi / 2.:
        # on practice it is unlikely to be triggered often
        print("big longitude difference - suspicious!!!")
        course = 360 - course

    if long12 == 0 and lat2 - lat1 == 0:
        return inf

    return course

######################## Auxiliary math ##############################

def series_distance(array):
    """
    Plain dumb summary length of an arbitrary array of points considered as a track. 
    """
    assert len(array) > 1
    return sum(
        [gpx_distance(array[i], array[i - 1]) for i in range(1, len(array))])


def angle_difference(a, b, full_circ=360.):
    """
    Return a difference between 2 angles considering circularity of a value. 
    """
    change = a - b
    if abs(a - b) == inf:
        return 0.0

    if change > full_circ / 2.:
        change = change - full_circ
    if change < -full_circ / 2.:
        change = change + full_circ
    return change

#NOTE: distances are relative to j-1, not j+1


def sampler_distance(gpx_segment, array, wingspan=3):
    """
    Samples a value divided by a summary distance of a sample.
    """

    results = []
    s = len(gpx_segment.points)
    for i in range(s):
        samples = []
        distance = 0
        for j in range(i - wingspan, i + wingspan + 1):
            if j in range(s):
                if j - 1 in range(s):
                    leg_length = gpx_distance(gpx_segment.points[j],
                                              gpx_segment.points[j - 1])
                    distance += leg_length
                    if leg_length > 1:#ignores duplicates
                        samples.append(array[j])
                    else:
                        samples.append(0.0)
                else:
                    samples.append(0.0)

        assert (distance != 0
                )  #singular distances might be 0 but not all of them
        results.append(sum(samples) / distance)

    return results


def sampler_distance_w_falloff(gpx_segment,
                               array,
                               wingspan=3,
                               falloff=5,
                               div_distance=True):  #ignores duplicates
    """
    Same as above, but the weight of each point in sample falls off exponentially with
    distance to a center point of a sample.
    falloff in km
    """
    results = []
    s = len(gpx_segment.points)
    for i in range(s):
        samples = []
        distance = 0
        for j in range(i - wingspan, i + wingspan + 1):
            if j in range(s):
                if j - 1 in range(s):
                    leg_length = gpx_distance(gpx_segment.points[j],
                                              gpx_segment.points[j - 1])
                    distance += leg_length
                    if leg_length > 1:
                        distance_to_center = gpx_distance(
                            gpx_segment.points[j], gpx_segment.points[i])
                        #print(distance_to_center)
                        samples.append(array[j] *
                                       exp(-distance_to_center / falloff))
                    else:
                        samples.append(0.0)
                else:
                    samples.append(0.0)

        assert (distance != 0
                )  #singular distances might be 0 but not all of them
        #results.append(sum(samples)/len(samples))
        results.append(
            sum(samples) / (distance if div_distance else len(samples)))

    return results



############################### FISHNESS CALCULATIONS #######################

def angle_variance(array):
    """
    circular variance
    (not really a circular dispersion, was a bit sleepy. But still useful. Dispersion is calculated similarly)
     https://en.wikipedia.org/wiki/Directional_statistics#Measures_of_location_and_spread
    """
    mean = np.array([angle_to_vector(x) for x in array])
    mean = np.average(mean, axis=0)
    #print(mean)
    variance = 1 - np.linalg.norm(mean)
    return variance


def angle_to_vector(angle):
    """
    Transforms an angle to a normalised 2D unity vector.
    This is for dispersion calcualtions of circualr values primarily.
    """
    return np.array([sin(angle / (2 * pi)), cos(angle / (2 * pi))])


def course_changes(gpx_segment):  # returned size is n-1
    """
    Produce an array of plain course changes based on logs.
    No divided-by-distance, nothing.
    """
    previous_course = -1
    course_changes = []
    for point in gpx_segment.points:
        if previous_course == -1:
            previous_course = point.course
            continue
        change = point.course - previous_course
        if change > 180:
            change = change - 360
        if change < -180:
            change = change + 360
        course_changes.append(abs(change))  #temporary
        previous_course = point.course

        #print(course_changes)
        return course_changes


def geometry_courses(gpx_segment):
    """
    True courses that are not based on logs but rather on neighbor points.
    """
    array = gpx_segment.points
    assert len(array) > 1
    res = [
        true_course_between_pts(array[i], array[i + 1])
        for i in range(0, len(array) - 1)
    ]
    
    ## Final point's course cannot be defined, so we just continue the trend
    res.append(res[-1])
    return res


def calculate_abs_change(array):
    """
    returns array of absolute changes of a given value.
    """

    return [0.0] + [
        abs(array[i] - array[i - 1]) 
        if not abs(array[i] - array[i - 1]) == inf else 0.0
        for i in range(1, len(array))
    ]


def calculate_circular_abs_change(array):
    """
    returns array of absolute changes of a given circular value.
    """

    return [0.0] + [
        abs(angle_difference(array[i], array[i - 1]))
        for i in range(1, len(array))
    ]


def geometry_coursechanges(gpx_segment, wingspan=3):
    """
    One of the functions for fishness calculations.
    Calculates changes (circular) in true courses that were derived from 
    geometry of a track.
    """

    courses = calculate_circular_abs_change(geometry_courses(gpx_segment))
    return sampler_distance_w_falloff(gpx_segment,
                                      courses,
                                      wingspan,
                                      falloff=10)
    return courses


def distances(gpx_segment):
    ## TODO: either delete or incorporate and reduce others.
    return [
        gpx_distance(gpx_segment.points[j], gpx_segment.points[j - 1])
        for j in range(1, len(gpx_segment))
    ]


def slowness(gpx_point, max_speed):
    """
    Rating of how slow ship moves compared to its max speed
    from 0 (fullspeed) to 1 (negligible)
    
    """

    sp = gpx_point.speed / max_speed
    return (exp(1 - sp) - 1) / (exp(1) - 1)


def slownesses(gpx_segment,max_speed, wingspan=3):
    """
    A quantitative measure of how slow the ship is moving for the purpose of 
    selection.
    """
    slows = [slowness(point, max_speed) for point in gpx_segment.points]
    return sampler_distance_w_falloff(gpx_segment,
                                      slows,
                                      wingspan,
                                      div_distance=False)



def circular_variances(gpx_segment, wingspan=3):  #returned size is n
    """
    Sampler for circular variance.
    Not distance-normalised.(Why?)
    """

    variances = []

    s = len(gpx_segment.points)
    for i in range(s):
        samples = []
        for j in range(i - wingspan, i + wingspan + 1):
            if j in range(s):
                samples.append(gpx_segment.points[j].course)
        variances.append(angle_variance(samples))

    return variances


def averaged_course_changes(gpx_segment, wingspan=3):  #returned size is n
    """
    moving average of course change modules, plain and simple
    """
    results = []

    s = len(gpx_segment.points)
    for i in range(s):
        samples = []
        for j in range(i - wingspan, i + wingspan + 1):
            if j in range(s):
                if j - 1 in range(s):
                    change = angle_difference(gpx_segment.points[j].course,
                                              gpx_segment.points[j - 1].course)
                    samples.append(abs(change))
                else:
                    samples.append(0.0)

        results.append(sum(samples) / len(samples))

    return results


def averaged_course_changes_w_distance(gpx_segment,
                                       wingspan=3):  #returned size is n
    """
    moving average of course change modules, distance sum is used instead of count
    """
    results = []

    s = len(gpx_segment.points)
    for i in range(s):
        samples = []
        distance = 0
        for j in range(i - wingspan, i + wingspan + 1):
            if j in range(s):
                if j - 1 in range(s):
                    change = angle_difference(gpx_segment.points[j].course,
                                              gpx_segment.points[j - 1].course)
                    samples.append(abs(change))
                    distance += gpx_distance(gpx_segment.points[j],
                                             gpx_segment.points[j - 1])
                else:
                    samples.append(0.0)
        assert (distance != 0
                )  #singular distances might be 0 but not all of them
        results.append(sum(samples) / distance)

    return results

def calculate_fields(gpx_segment,zonespecs):
    """
    main calculation function
    """
    
    df = segment_to_pandas(gpx_segment)

    df = df.assign(avg_course_change=pd.Series(
            averaged_course_changes_w_distance(gpx_segment, 4)))
    df = df.assign(g_course_change=pd.Series(geometry_coursechanges(gpx_segment)))
    df = df.assign(g_course=pd.Series(geometry_courses(gpx_segment)))
    
    MAX_SPEED = max(df['speed'].to_list())
    df = df.assign(avg_slow=pd.Series(slownesses(gpx_segment, MAX_SPEED)))
    
    for zone in zonespecs:
        df = df.assign(**{zone.name: zone.rating_series(df)})
    
    return df
    

######################## LOGIC ############################################

def ramp(array, thres=0):
    """
    Same thing as a color ramp filter.
    Makes a 1/0 array from a gradually changing value.
    """
    return [1. if x > thres else 0. for x in array]


def filter_length(array, min_):
    """
    in an array of 1s and 0s filters 1s that do not form a sequence
    """

    res = []
    count = 0

    for i, value in enumerate(array):
        #print (value)
        res.append(value)
        if value == 0:
            if count != 0:  # detecting edge
                #print("sequence of ", count )
                if count < min_:
                    print("deleting too short", count)
                    for j in range(i - count, i):
                        res[j] = 0
            count = 0

        else:
            count += 1
    return res


def heal(array, min_):
    """
    healing stray 0s across segmens of 1s
    """
    return inverse(filter_length(inverse(array), min_))


def inverse(array):
    """
    1s are now 0 and vice versa
    """
    return [1 if x == 0 else 0 for x in array]


def separate_segment(gpx_segment, separator_array):
    """
    separator array can be anything, not necessarily 0 and 1
    Separation goes between different values of separator array
    """

    if len(separator_array) == 0:
        print("received an empty separator")
        return []
    
    #print("separating using an array of len", len(separator_array),"and segment of len", len(gpx_segment.points))
    #print("last point in segment is", gpx_segment.points[-1])

    res = []

    count = 0
    val = separator_array[0]
    #print ("val =", val)

    for i, value in enumerate(separator_array):
        #print (value)
        if value != val:
            if count != 0:  # detecting edge
                print("sequence of ", count, val, len(res))
                new_segment = SpecializedSegment(val)
                #for j in range(i-count-(1 if i-count-1>0 else 0),i):##we NEED to include the last point of previous segment
                #actually no. "glue" things will be special and visual only
                for j in range(i - count, i):
                    new_segment.points.append(gpx_segment.points[j])
                res.append(new_segment)
            count = 1
            val = value
        else:
            count += 1
            
    ## deal with the end
    print("sequence of ", count, val, len(res))
    if count!=0:#Probably redundant check
        new_segment = SpecializedSegment(val)
        new_segment.points = gpx_segment.points[-count:]
        res.append(new_segment)
    return res

################# CONVERSIONS ##############################################3

def gpx_to_location(gpx_point):
    """
    returns a location of the point as an array
    for interoperability between folium and gpxpy
    """

    return [gpx_point.latitude, gpx_point.longitude]


def gpx_point_to_array(gpx_point):
    """
    returns a location of the point as an array
    for interoperability between seanoodle,mpl,shapely stuff and gpxpy
    x is FIRST compared to folium format
    """
    return [gpx_point.longitude, gpx_point.latitude]

def segment_to_pandas(gpx_segment):
    """
    Converts a gpxpy.gpx.GPXTrackSegment to a dataframe
    """
    points = gpx_segment.points
    df = pd.DataFrame({'latitude': [x.latitude for x in points], 
                       'longitude': [x.longitude for x in points],
                       'course': [x.course for x in points],
                       'speed': [x.speed for x in points],
                       'time': [x.time for x in points],
                       'description': [x.description for x in points],
                       'comment': [x.comment for x in points],
                       })
    
    return df

def pandas_to_segment(df):
    """
    Extracts a gpxpy.gpx.GPXTrackSegment from a dataframe ignoring non-gpx fields
    """
    
    gpx_segment = GPX.GPXTrackSegment()
    
    for tup in df.itertuples(): # actually one can do it via setattr in a cycle
        point = GPX.GPXTrackPoint()
        point.latitude = tup.latitude
        point.longitude = tup.longitude
        point.course = tup.course
        point.speed = tup.speed
        point.time = tup.time
        point.description = tup.description
        point.comment = tup.comment
        gpx_segment.points.append(point)
    
    return gpx_segment

########################## SEGMENTATION ###################################

def find_negative_segment_groups(classified_segments):
    """
    find negative segments and their enroutes
    """
    
    negative_segments = []
    sequence_count = 0
    negative_present = False
    for i, segment in enumerate(classified_segments):
        if segment.role in POSITIVE_ACTIVITIES:
            if negative_present:
                negative_segments.append(classified_segments[i - sequence_count:i])
            sequence_count = 0
            negative_present = False
        else:
            if segment.role in NEGATIVE_ACTIVITIES:
                negative_present = True
            sequence_count += 1
            
    ## Adding a possible ending negative segment
    if negative_present:
                negative_segments.append(classified_segments[-sequence_count:])
    
    return negative_segments

def segmentize_by_task(df, zonespecs, control_points):
    """
    Segmenting initial track into task-specific segments
    """
    
    zonemasks = [{'name':x.name,'mask':x.zone_mask(df)} for x in zonespecs]
    gpx_segment = pandas_to_segment(df) 
    gpx_points = gpx_segment.points
    
    for i, point in enumerate(gpx_points):
        in_zone = False
        if any([cp.is_in_detection_range(point) for cp in control_points]):
            point.comment = "control"
            continue
        for zm in zonemasks:
            if zm['mask'][i]==1:
                point.comment = zm['name']
                in_zone=True
                break
            
        if not in_zone:
            point.comment = "enroute"
    
    classified_segments = separate_segment(
        gpx_segment, [point.comment for point in gpx_points])
    #add missing glue "enroutes" (segment roles are not of one rank. enroute is special)
    for i, segment in enumerate(classified_segments):
        if segment.role == "enroute":
            if i > 0:
                segment.points.insert(0, classified_segments[i - 1].points[-1])
            if i < len(classified_segments) - 1:
                segment.points.append(classified_segments[i + 1].points[0])
    
    return classified_segments

def report_negative_segments(negative_segments):
    """
    Print basic info about an array of negative segments
    """
    print("negative segments:")
    for group in negative_segments:
        print("Negative group of", len(group))
        for s in group:
            print("\t", s.role)
        print("summary length is", sum([s.length_2d() for s in group]) / 1000)

    
        

################## ROUTING #################################################

def try_route_better_than_group(segment_group, route_optimality = 1):
    """
    Invokes SeaNoodle to make a route alternative to a given group of GPXTrackSegments
    route_otimality regulates how eagerly seanoodle will try to reach the goal.
    Lower values - somewhat unoptimal result but much faster in many cases.
    """
    
    start_point = gpx_point_to_array(segment_group[0].points[0])
    end_point = gpx_point_to_array(segment_group[-1].points[-1])
    print("BRACE FOR IMPACT. ROUTING BETWEEN POINTS", start_point, end_point)
    route = seanoodle.route(start_point, end_point, 0.02, route_optimality)
    route.description="Original length:"+str(sum([s.length_2d() for s in segment_group]) / 1000)
    
    return route

####################### PANDAS #############################################

def selective_dot(df,other_df):
    """
    A regular dot operation, but in the first df all columns that are not in
    the index of the second df are ignored, thus avoiding "not aligned" error
    """
    
    return df[other_df.index.tolist()].dot(other_df)        
    

##################### ZONES ###############################################

class ZoneSpec(object):
    """
    A base class for activity zones (like fishing)
    """
    
    def __init__(self, name, weights, thres):
        self.name = name
        self.weights = weights # zone rating is calculated as f1*w1+f2*w2...fn*wn. Weights are a dictionary fn:wn.
        self.thres = thres
    
    def rating_series(self, df):
        """
        calculate a rating function by summing over other functions with weights.
        Dot operation is used instead of plain sum for speed.
        """
        weights_df = pd.DataFrame(self.weights, index=[0]).transpose()
        
        return selective_dot(df,weights_df)
    
    def zone_mask(self,df):
        rating = self.rating_series(df)[0].to_list()
        #print (rating)
        return filter_length(heal(ramp(rating, thres=self.thres), 3), 2)

def extra_label_df(df,zonespecs):
    """
    Compile extra human-readable information about numbers behind each zone rating calculation.
    For easy debugging.
    """
    
    dic = {}
    for zone in zonespecs:
        dic['SUMMARY_'+zone.name] = df[zone.name]
        for key in zone.weights.keys():
            dic[zone.name+'_'+key] = df[key].to_numpy()*zone.weights[key]
    return pd.DataFrame(dic)


################################# ZONE DECLARATIONS #########################

FISHING_ZONESPEC = ZoneSpec('fishing',
                             {
                                     'avg_course_change' : 1,
                                     'g_course_change' : 1,
                                     'avg_slow' : 120,
                                     },
                                     20)
zonespecs = [FISHING_ZONESPEC]

### Parse json of control points. They don't change so global
control_points = load_control_points('points.json')

######################################################### MAIN ####################
### Open and parse gpx file

if __name__ == '__main__':
    
    if len(argv) < 3 or argv[1] in ["-h", "--help"]:
        usage()
        exit(0)
    
    with open(argv[1], 'r') as gpx_file:
        gpx = gpxpy.parse(gpx_file)
    
    
    ### Navigation shortcuts
    gpx_segment = gpx.tracks[0].segments[0]
    ## Arranging points chronologically to avoid bs.
    gpx_segment.points.sort(key=lambda x: x.time) 
    gpx_points = gpx_segment.points
    
    ################## PLAYING WITH STUFF HERE ##############################
    
    df = calculate_fields(gpx_segment,zonespecs)
    print(df)
    
    classified_segments = segmentize_by_task(df,zonespecs,control_points)
    
    negative_segments = find_negative_segment_groups(classified_segments)
    report_negative_segments(negative_segments)
    
    #optimised_routes = []
    optimised_routes = [try_route_better_than_group(group) for group in negative_segments]
    
    
    m = make_a_map(control_points, df, zonespecs, classified_segments, optimised_routes)
    m.save(argv[2])
    print("success")
    exit(0)
