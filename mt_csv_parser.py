#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#import gpxpy
from gpxpy import gpx as GPX
from segmentor import gpx_distance
import csv
import json
import geojson
import sqlite3
from pathlib import Path
from sys import argv
#import glob
from datetime import datetime

PORT_DETECTION_RADIUS_KM = 5.
EARTH_RADIUS = 6371.
# it doesn't need to be spherically precise and distances are very small.
ROUGH_PORT_ANGULAR_RADIUS = PORT_DETECTION_RADIUS_KM/(EARTH_RADIUS*2*3.14)*360  

def init_db():
    """
    Initialise a database.
    Returns a new connection. 
    """
    con = sqlite3.connect(":memory:")
    con.execute('''CREATE TABLE ports
            (x float, y float, name string)''')
    return con

def add_port(coords, name, con):
    #print ("adding port: ", coords, name)
    con.execute("INSERT INTO ports VALUES (?,?,?)",
                (coords[0], coords[1], name,))

def is_in_port(point, con):
    """
    returns name of the port or false
    """
    
    ports = con.execute(
        '''SELECT x, y, name
                            FROM ports
                            WHERE x BETWEEN ? AND ? AND y BETWEEN ? AND ?  ''',
        (   point.longitude - ROUGH_PORT_ANGULAR_RADIUS,
            point.longitude + ROUGH_PORT_ANGULAR_RADIUS,
            point.latitude - ROUGH_PORT_ANGULAR_RADIUS,
            point.latitude + ROUGH_PORT_ANGULAR_RADIUS))
    
    ports = list(ports) 
    if len(ports) == 0:
        return False
    return  ports[0][2]
    

def usage():
    print("Usage:\t %s INFILE.csv " % argv[0])

def load_ports(filename, con):
    """
    Auxiliary file handing function to load ports from a json fle. 
    """
    
    with open(filename) as file:
        gj = geojson.load(file)

    features = gj.features
    for feature in features:
        coords = feature.geometry.coordinates
        name = feature.properties['portname']
        add_port(coords, name, con)

    return con

def read_and_slice(filename):
    
    with open(filename, 'r') as csv_file:
        lines = csv.DictReader(csv_file)
        
        segments = []
        sequence = []
        previous_port_line = None
        for line in lines:
            
            ## note that arrival and departure points themselves are not consumed
            ## These points do not have any location so they are quite useless 
#            if line["Event"] in ["Departure", "Arrival"]:
#                if len(sequence)!=0:
#                    segments.append(sequence)
#                sequence=[]
#            else:
#                sequence.append(line)
            
            if line["Event"] in ["Departure", "Arrival"]:
                continue
            
            point = line_to_gpx(line) 
            port = is_in_port(point, port_db_con)
            if port:
                print ("port detected!", port)
                if len(sequence)!=0:
                    sequence.append(line)
                    segments.append(sequence)
                sequence=[]
                previous_port_line = line
                
            elif previous_port_line!=None :
                #print("adding previous_port_line",previous_port_line)
                sequence.append(previous_port_line)
                sequence.append(line)
                previous_port_line = None
                #print ("first member of a sequence is", sequence[0])
            else:
                sequence.append(line)
            
        
        if len(sequence)!=0:
                segments.append(sequence)
    
    print("returning ", len(segments), "segments")
    return segments

def line_to_gpx(line):
    point = GPX.GPXTrackPoint()
    
    point.latitude = float(line['Lat'])
    point.longitude = float(line['Lon'])
    point.description = line['Event'] + ":" + line['Event Content']
    point.course = float(line['Course'])
    point.speed = float(line['Speed'])
    point.time = datetime.strptime(line['Date Time'],"%Y-%m-%d %H:%M:%S")
    
    return point

def to_gpx_track(csv_lines):
    print ("to_gpx_track, starting with", csv_lines[0])
    track = GPX.GPXTrack()
    
    ## A bit of an artifact in the first dict key
    vessel_name_key = [x for x in csv_lines[0].keys() if 'Vessels' in x][0]
    
    track.comment = csv_lines[0][vessel_name_key].replace(' ','_')
    segment = GPX.GPXTrackSegment()
    track.segments.append(segment)
    for line in csv_lines:
        segment.points.append(line_to_gpx(line))
    
    return track

def tracks_from_file(filename):
    
    return [to_gpx_track(group) for group in read_and_slice(filename)]

port_db_con = init_db()
base_path = Path(__file__).parent
#ports_path = (base_path /
#                  "thirdparty/sea-ports/lib/ports.json").resolve()
ports_path = (base_path /
                  "thirdparty/wfp-seaports/ports.json").resolve()
extra_ports_path = (base_path / "extra_ports.json").resolve()

load_ports(ports_path, port_db_con)
load_ports(extra_ports_path, port_db_con)
    
######################################################### MAIN ####################
### Open and parse csv file

if __name__ == '__main__':
    
    if len(argv) < 2 or argv[1] in ["-h", "--help"]:
        usage()
        exit(0)
    
    filename = argv[1]
    gpx = GPX.GPX()
    for track in tracks_from_file(filename):
        gpx.tracks.append(track)
    #print(gpx.to_xml(version="1.0"))