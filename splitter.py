#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
splits MarineTraffic "Date Time" column into two

usage: 
    $ cat file.csv | ./splitter.py > new_file.csv
    
"""
from sys import stdin

while True:
    line = stdin.readline().rstrip('\n')
    if len(line)>0:
        tokens = line.split(sep=",")
        tokens2 = line.split(sep=";")
        if len(tokens2)>len(tokens):
            tokens = tokens2
        date,time = tokens[2].split(sep=" ")
        tokens.remove(tokens[2])
        tokens.insert(2,date)
        tokens.insert(2,time)
        res = ",".join(tokens)
        print (res)
    else:
        break

