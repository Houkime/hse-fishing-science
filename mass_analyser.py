#!/usr/bin/python3

import pandas as pd
from os import path, remove, makedirs
import datetime
import geojson
import sqlite3

#from itertools import chain
from sys import argv
#import gpxpy

import mt_csv_parser
import segmentor as sgr

WASTE_KM_THRESHOLD = 50


def init_db():
    """
    Initialise a database.
    Returns a new connection. 
    """
    con = sqlite3.connect(":memory:")
    con.execute('''CREATE TABLE points
            (x float, y float, name string)''')
    return con

def usage():
    print("Usage: INFILES.csv -o OUTFOLDER")

def process_track(track, out_file):
    ### Navigation shortcuts
    gpx_segment = track.segments[0]
    ## Arranging points chronologically to avoid bs.
    gpx_segment.points.sort(key=lambda x: x.time) 
    
    
    df = sgr.calculate_fields(gpx_segment,sgr.zonespecs)
    print(df)
    
    classified_segments = sgr.segmentize_by_task(df,sgr.zonespecs,sgr.control_points)
#    for i, segment in enumerate(classified_segments):
#        point = segment.points[-1]
#        print("last point of", i, "segment", point, segment.role)
    negative_segments = sgr.find_negative_segment_groups(classified_segments)
    sgr.report_negative_segments(negative_segments)
    
    optimised_routes = []
    
    for group in negative_segments:
        
        route = sgr.try_route_better_than_group(group, route_optimality=0.7) 
        optimised_routes.append(route)
        original_start_time = group[0].points[0].time
        original_end_time = group[-1].points[-1].time
        original_delta_hours = (original_end_time - original_start_time).total_seconds()/3600
        original_length = float(route.description.split(sep=":")[1])
        
        final_length = sgr.series_distance(route.points)
        max_speed =(df['speed'].max()*sgr.NAUTICAL_MILE_IN_M/1000)
        optimised_delta_hours = final_length/max_speed
        difference = original_length - final_length
        time_difference = original_delta_hours - optimised_delta_hours
        has_tremedous_ineconomy = False
        if difference>=WASTE_KM_THRESHOLD:
            has_tremedous_ineconomy = True
            
        dic = {'time_start':original_start_time,
                              'lat_start':route.points[0].latitude,
                              'lon_start':route.points[0].longitude,
                              'lat_end':route.points[1].latitude,
                              'lon_end':route.points[1].longitude,
                              'original_length':original_length,
                              'final_length':final_length,
                              'difference':difference,
                              'wasteful':has_tremedous_ineconomy,
                              'wasteful_threshold':WASTE_KM_THRESHOLD,
                              'max_speed_seen_kmh': max_speed,
                              'original_hours': original_delta_hours,
                              'opt_hours_maxspeed': optimised_delta_hours,
                              'est_time_economy': time_difference,
                              'filename':out_file,
                              }
        optimisations.append(dic)
    
    m = sgr.make_a_map(sgr.control_points, 
                       df,
                       sgr.zonespecs,
                       classified_segments,
                       optimised_routes)
    if path.exists(out_file):
        print("removing previous outfile", out_file)
        remove(out_file)
    m.save(out_file)
    print("saved", out_file)


optimisations = []              

if __name__ == '__main__':
    
    try:
        output_index = argv.index("-o")
    except:
        usage()
        exit(1)
        
    input_files = argv[1:output_index]
    print (input_files)
    output_path = argv[output_index+1]    
    makedirs(output_path,exist_ok = True)

    for file in input_files:
        tracks = mt_csv_parser.tracks_from_file(file)
        for i,track in enumerate(tracks):
            output_filepath = path.join(output_path,track.comment+'-'+str(i)+'.html')
            print("making ", output_filepath)
            process_track(track,output_filepath)

    optimisation_df = pd.DataFrame(optimisations)
    print ("Optimisation report:")
    print (optimisation_df)
    report_path = path.join(output_path,'optimisation_report.csv')
    print("saving this report as", report_path)
    with open(report_path,mode='w') as file:
        file.write(optimisation_df.to_csv())
        file.flush()
        