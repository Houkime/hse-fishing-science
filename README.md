Scripts for processing maritime GPS tracking data and measuring economic losses from inefficient laws.

To convert non-standard MarineTraffic datat to gpx, use

```
cat input.csv | ./splitter.py | gpsbabel -t -i unicsv -o gpx -f - -F - > out.gpx
```

### Dependencies:

* pandas
* folium
* gpxpy
* seanoodle (for routing, https://gitlab.com/Houkime/sea-noodle)
* numpy

### Third-party datasets:

* Global Ports Dataset by WFP - https://data.humdata.org/dataset/global-ports (CC-BY-SA http://creativecommons.org/licenses/by-sa/4.0/legalcode)

### Scripts:

### segmentor.py
Used alone divides track stored  in.gpx file into activity segments (currently heuristically deduced fishing and file-defined russian FSB control point visits are detected).
Renders the result into an html file (with a folium map).  
It also can optimise out excessive bureaucratic activities using SeaNoodle marine routing engine (https://gitlab.com/Houkime/sea-noodle).

Example syntax:

```
$ ./segmentor.py INPUT.gpx OUTPUT.html
```

### mt_csv_parser.py

Is a MarineTraffic-specific module to extract GPXTracks from csv. It returns multiple tracks with separation based on port departure and arrival. 
So it is possible to analyse long files one port-port journey at a time.

### mass_analyser.py

Uses aforementioned segmentor and parser to mass - analyse a heap of csv files.  

Example syntax:

```
$ ./mass_analyser.py inputs/*.csv -o outputs/

```

Where after `-o` goes a folder where results will be stored.
Results include:

* Per-voyage folium htmls. (rewritten every time analyser is invoked)
* Summary report on bureaucratic activities and optimisation results.

### Notes:

gpxpy is under APACHE 2.0 license and is used without modification.